#include "MessageSender.h"
#include <iostream>

using std::cout;
using std::endl;

int main()
{
    int testResult;

    try
    {
        std::cout << "Trying to create a new MessageSender object... \n \n" << std::endl;
        MessageSender ms;
        cout << "\033[1;32mOK\033[0m\n \n" << endl;
        testResult = 0;
    }
    catch (...)
    {
        cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
        std::cout << " \n" << endl;
        system("./printMessage 12201");
        std::cout << " \n" << endl;
		return 12201;
    }

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex12 Part 2 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx12 Part 2 Tests Failed\033[0m\n \n") << endl;
    return testResult;
}