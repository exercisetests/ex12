#include "Threads.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>

using std::cout;
using std::endl;

//int getNumOfLinesInFile(const std::string fileName)
//{
//	std::string line;   // To read each line from code
//	int count = 0;    // Variable to keep count of each line
//
//	std::ifstream mFile(fileName);
//
//	if (mFile.is_open())
//	{
//		while (mFile.peek() != EOF)
//		{
//			getline(mFile, line);
//			count++;
//		}
//		mFile.close();
//
//		return count;
//	}
//	else
//	{
//		return -1;
//	}
//}

std::string readFileToString(const std::string fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

void initNumbersSetFromFile(std::set<std::string>& numbersSet, const std::string fileName)
{
	std::string fileContent = readFileToString(fileName);

	std::istringstream sstream(fileContent);
	std::string line;
	while (std::getline(sstream, line))
	{
		numbersSet.insert(line);
	}
}

int main()
{
    int testResult;
    const std::string fileName = "primes1000000.txt";
	const std::string testFileName = "primes1000000Test.txt";

    try
    {
		cout <<
			"\ncalling callWritePrimesMultipleThreads(1, 1000000, \"primes1000000\", 6)... \n" << endl;

        callWritePrimesMultipleThreads(1, 1000000, fileName, 6);

		std::set<std::string> testSet;
		initNumbersSetFromFile(testSet, testFileName);

		std::set<std::string> numbersSet;
		initNumbersSetFromFile(numbersSet, fileName);

		int numberOfLinesInFile1 = numbersSet.size();
		int numberOfLinesInFile2 = testSet.size();

		if (numberOfLinesInFile1 < 0)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 12101");
			std::cout << " \n" << endl;
			return 12101;
		}

		if (numberOfLinesInFile1 != numberOfLinesInFile2)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 12102");
			std::cout << " \n" << endl;
			return 12102;
		}

		std::cout << "Numbers found in file --- " << numberOfLinesInFile1 << std::endl;

		cout << "\033[1;32mOK\033[0m\n \n" << endl;
		testResult = 0;
        
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex12 Part 1 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx12 Part 1 Tests Failed\033[0m\n \n") << endl;
    return testResult;
}